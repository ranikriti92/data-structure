package com.ds.linkedlist;

import java.util.Stack;

public class PalindromeLinkedList {

	public boolean isPalindrome(Node head) {

		Stack<Integer> stack = new Stack<Integer>();
		Node temp = head;
		while (temp != null) {
			stack.push(temp.data);
			temp = temp.next;
		}
		temp = head;
		boolean isPalindrome = false;
		while (temp != null) {
			if (temp.data == stack.pop()) {
				temp = temp.next;
				isPalindrome = true;
			} else {
				isPalindrome = false;
				break;
			}

		}
		return isPalindrome;
	}

	public static void main(String args[]) {
		CustomLinkedList obj = new CustomLinkedList();
		obj.insert(1);
		obj.insert(2);
		obj.insert(3);
		obj.insert(2);
		obj.insert(1);
		System.out.println(obj.isPalindrome());
	}

}
