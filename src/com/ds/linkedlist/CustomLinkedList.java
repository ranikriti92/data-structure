package com.ds.linkedlist;

import java.util.Stack;

public class CustomLinkedList {
	Node head;

	public void insert(int data) {
		Node newNode = new Node(data);
		if (head == null)
			head = newNode;
		else {
			Node traverse = head;
			while (traverse.next != null) {
				traverse = traverse.next;
			}
			traverse.next = newNode;
		}
	}

	public void insertAtBeg(int data) {
		Node newNode = new Node(data);
		newNode.next = head;
		head = newNode;
	}

	public void insertAtIndex(int index, int data) {

		if (index == 0)
			insertAtBeg(data);
		else {
			Node newNode = new Node(data);
			Node temp = head;
			int i = 0;
			while ((i < index - 1) && temp.next != null) {
				temp = temp.next;
				i++;
			}

			newNode.next = temp.next;
			temp.next = newNode;
		}

	}

	public void delete() {
		if (head == null) {
			System.out.println("List is empty");
			return;
		}
		head = head.next;
	}

	public void deleteAtIndex(int index) {

		if (index == 0)
			delete();
		else {

			Node temp = head;
			for (int i = 0; i < index - 1; i++) {
				temp = temp.next;
			}

			temp.next = temp.next.next;

		}

	}

	public void print() {
		if (head == null) {
			System.out.println("List is empty");
			return;
		}
		Node temp = head;
		while (temp != null) {
			System.out.print(temp.data + " ");
			temp = temp.next;
		}
		System.out.println();
	}

	public boolean isPalindrome(Node head) {

		Stack<Integer> stack = new Stack<Integer>();
		Node temp = head;
		while (temp != null) {
			stack.push(temp.data);
			temp = temp.next;
		}
		temp = head;
		boolean isPalindrome = false;
		while (temp != null) {
			if (temp.data == stack.pop()) {
				temp = temp.next;
				isPalindrome = true;
			} else {
				isPalindrome = false;
				break;
			}

		}
		return isPalindrome;
	}

	public boolean isPalindrome() {
		boolean flag =isPalindrome(head); 
		return flag;
	}

}
