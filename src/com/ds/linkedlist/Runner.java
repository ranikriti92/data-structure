package com.ds.linkedlist;

public class Runner {

	public static void main(String[] args) {
		CustomLinkedList customLinkedList = new CustomLinkedList();
		customLinkedList.insert(15);
		customLinkedList.insert(20);
		customLinkedList.insertAtIndex(2, 25);
		customLinkedList.insertAtIndex(3, 28);
		customLinkedList.insert(35);
		customLinkedList.insertAtIndex(10, 40);
		customLinkedList.print();
		customLinkedList.delete();
		customLinkedList.print();
		customLinkedList.deleteAtIndex(2);
		customLinkedList.print();
		
		
	}

}
