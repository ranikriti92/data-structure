package com.ds.ArrayList;

public class ArrayListRunner {

	public static void main(String[] args) {
		CustomArrayList<String> obj = new CustomArrayList<String>(); 
		obj.add("A");
		obj.add("B");
		obj.add("C");
		obj.add("D");
		obj.add("E");
		obj.add("F");
		obj.remove(4);
		System.out.println(obj.get(4));
		obj.print();
		obj.remove("C");
		obj.print();

	}

}
