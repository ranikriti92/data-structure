package com.ds.ArrayList;

import java.util.Arrays;

public class CustomArrayList<E> {

	private static final int INITIAL_CAPACITY = 5;
	private int size = 0;
	private Object data[] = {};

	CustomArrayList() {
		data = new Object[INITIAL_CAPACITY];
	}

	public void add(E e) {
		if (size == data.length) {
			System.out
					.println("The array has been reached to its maximum size hence increasing the size to its double ");
			ensureCapacity();
		}
		data[size++] = e;
	}

	private void ensureCapacity() {
		data = Arrays.copyOf(data, data.length * 2);
	}

	public E get(int index) {
		if (index < 0 || index >= size)
			throw new ArrayIndexOutOfBoundsException(" Index " + index + "  Array Size is " + (size-1));
		return (E) data[index];
	}

	public void remove(int index) {
		if (index < 0 || index >= size)
			throw new ArrayIndexOutOfBoundsException(" Index " + index + "Array Size is " + size);

		for (int i = index; i < size - 1; i++) {
			data[i] = data[i + 1];

		}
		size--;
	}

	public void remove(Object obj) {

		for (int i = 0; i < size - 1; i++) {

			if (data[i] == obj) {
				for (int j = i; j < size - 1; j++) {
					data[j] = data[j + 1];
				}
				break;
			}
		}
		size--;

	}

	public void print() {

		for (int i = 0; i < size; i++) {

			System.out.print(data[i] + " ");
		}
		System.out.println();

	}

}
